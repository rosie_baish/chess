#!/usr/bin/python3

import subprocess

proc = subprocess.Popen("./chess --quiet --uci --new-game games/pipe_test.chess",
                        shell=True,
                        stdin=subprocess.PIPE,
                        stdout=subprocess.PIPE)

cppMessage = proc.stdout.readline().decode('utf-8').rstrip()
print(cppMessage)

state = "run"
while state == "run":
    std_in = input()

    if std_in == "":
        state = "terminate" # any string other than "run" will do
        break

    std_in = std_in + "\n"
    proc.stdin.write(std_in.encode('utf-8'))
    proc.stdin.flush()

    cppMessage = proc.stdout.readline().decode('utf-8').rstrip()
    print(cppMessage)
