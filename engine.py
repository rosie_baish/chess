#!/usr/bin/python3

import berserk
import subprocess
import threading
import os

my_move = False #True

class Game(threading.Thread):
    def __init__(self, client, game_id, colour, **kwargs):
        super().__init__(**kwargs)
        # Need new-file if we are not resuming a game.
        self.colour = colour
        self.filename = "games/lichess{}.chess".format(game_id)

        exists = os.path.isfile(self.filename)
        if (exists):
            i = 0
            with open(self.filename) as f:
                for i, l in enumerate(f, 1):
                    pass

            if (i % 2 == 1):
                self.colour_to_move = "white"
            else:
                self.colour_to_move = "black"

        else:
            f = open(self.filename,"w+")
            f.write("start\n")
            f.close()
            self.colour_to_move = "white"

        self.game_id = game_id
        self.client = client
        self.stream = client.bots.stream_game_state(game_id)
        self.current_state = next(self.stream)

    def run(self):
        command = ["./chess", "--quiet", "--uci",
                   "--{}".format(self.colour),
                   "{}".format(self.filename)]

        self.proc = subprocess.Popen(command,
                                     stdin=subprocess.PIPE,
                                     stdout=subprocess.PIPE)

        print("(python) " + str(command))

        if (self.colour == self.colour_to_move):
            ret = self.make_move()
            if (ret):
                return

        for event in self.stream:
            print("(python)" + str(event))
            if event['type'] == 'gameFull':
                """
                if (event['white']['id'] == "Light_Blue_bot"):
                    self.colour = "white"
                else:
                    self.colour = "black"

                self.proc = subprocess.Popen(["./chess", "--quiet", "--uci",
                                              "--{}".format(self.colour),
                                              "{}".format(self.filename)],
                                             stdin=subprocess.PIPE,
                                             stdout=subprocess.PIPE)
                if (self.colour == "white"):
                    self.make_move()
"""
                pass
            elif event['type'] == 'gameState':
                ret = self.handle_state_change(event)
                if (ret):
                    return
            elif event['type'] == 'chatLine':
                self.handle_chat_line(event)

    def handle_state_change(self, game_state):
#        print("(python) " + str(game_state))
        moves_list = game_state['moves']
        move_array = moves_list.split()
        last_move = move_array[-1]

        if (len(move_array) % 2 == 0):
            last_move_by = "black"
        else:
            last_move_by = "white"

        if (self.colour != last_move_by):
            print("(python) Opponent made the move: " + last_move)
            last_move = last_move + "\n"
            self.proc.stdin.write(last_move.encode('utf-8'))
            self.proc.stdin.flush()

            return self.make_move()

        else:
            print("(python) LightBlue made the move: " + last_move)

        return False # Continue executing

    def handle_chat_line(self, chat_line):
        print("(python) " + chat_line)

    def make_move(self):
        our_move = self.proc.stdout.readline().decode('utf-8').rstrip()

        if (self.proc.poll != None):
            print("(python) Poll True")
        else:
            print("(python) Poll False")

        if (our_move == ""):
            print("(python) Error - blank move received. ")
            print("(cpp stdout) " +
                  self.proc.stdout.readline().decode('utf-8').rstrip())
            print("(cpp stdout) " +
                  self.proc.stdout.readline().decode('utf-8').rstrip())
            print("(cpp stdout) " +
                  self.proc.stdout.readline().decode('utf-8').rstrip())
            return True
        else:
            print("(python) Our Response: #{}#".format(our_move))
            client.bots.make_move(self.game_id, our_move)
            return False


with open('.token') as f:
    token = f.read()

session = berserk.TokenSession(token)
client = berserk.Client(session)

last_challenge = ""
last_challenge_colour = ""

default_colour = "white"
#"black"
#"white"

print("(python) Chess Engine Ready")

for event in client.bots.stream_incoming_events():
    print("(python) " + str(event))
    if event['type'] == 'challenge':
        if (event['challenge']['color'] == "random"):
            client.bots.reject_challenge(event['challenge']['id'])
        else:
            client.bots.accept_challenge(event['challenge']['id'])
            last_challenge = event['challenge']['id']
            last_challenge_colour = event['challenge']['color']
    elif event['type'] == 'gameStart':
        if (event['game']['id'] == last_challenge):
            game = Game(client, event['game']['id'], last_challenge_colour)
            last_challenge = ""
        else:
            game = Game(client, event['game']['id'], default_colour)
        game.start()
