#include <string>
#include <iostream>

#include "piece.h"

using namespace std;

// Set the flag
bool Piece::lookup_filled = false;
map<string, Piece::Value> Piece::string_lookup;
void Piece::fill_lookup() {
    if (!Piece::lookup_filled) {
        Piece::string_lookup["BR"] = BLACK_ROOK;
        Piece::string_lookup["BN"] = BLACK_KNIGHT;
        Piece::string_lookup["BB"] = BLACK_BISHOP;
        Piece::string_lookup["BQ"] = BLACK_QUEEN;
        Piece::string_lookup["BK"] = BLACK_KING;
        Piece::string_lookup["BP"] = BLACK_PAWN;
        
        Piece::string_lookup["WR"] = WHITE_ROOK;
        Piece::string_lookup["WN"] = WHITE_KNIGHT;
        Piece::string_lookup["WB"] = WHITE_BISHOP;
        Piece::string_lookup["WQ"] = WHITE_QUEEN;
        Piece::string_lookup["WK"] = WHITE_KING;
        Piece::string_lookup["WP"] = WHITE_PAWN;
 
        Piece::string_lookup["  "] = EMPTY;
        lookup_filled = true;
    }
}

Piece::Piece(string piece_val) {
    Piece::fill_lookup();
    if (string_lookup.find(piece_val) != string_lookup.end()) {
        value = string_lookup[piece_val];
    } else {
        cerr << "No such piece value '" << piece_val << "'" << endl;
        exit(-1);
    }
}

std::string Piece::get_piece_string() const {
    switch (value) {
        case (BLACK_ROOK):
            return "BR";
            break;
        case (BLACK_KNIGHT):
            return "BN";
            break;
        case (BLACK_BISHOP):
            return "BB";
            break;
        case (BLACK_KING):
            return "BK";
            break;
        case (BLACK_QUEEN):
            return "BQ";
            break;
        case (BLACK_PAWN):
            return "BP";
            break;
            
        case (WHITE_ROOK):
            return "WR";
            break;
        case (WHITE_KNIGHT):
            return "WN";
            break;
        case (WHITE_BISHOP):
            return "WB";
            break;
        case (WHITE_KING):
            return "WK";
            break;
        case (WHITE_QUEEN):
            return "WQ";
            break;
        case (WHITE_PAWN):
            return "WP";
            break;

        case (EMPTY):
            return "  ";
            break;
            
        default:
            return "XX";
            break;
    }
    return "XX";
}

// Format for the options is:
// { d_row, d_col, max_distance }

int Piece::black_pawn_opts[3][3] = {
    {-1, 0, 2},
    // Taking only
    {-1,  1, 1},
    {-1, -1, 1}
};

int Piece::white_pawn_opts[3][3] = {
    {1, 0, 2},
    // Taking only
    { 1,  1, 1},
    { 1, -1, 1}
};

int Piece::bishop_opts[4][3] = {
    {1, 1, 8},
    {1, -1, 8},
    {-1, 1, 8},
    {-1, -1, 8}
};

int Piece::rook_opts[4][3] = {
    {1, 0, 8},
    {-1, 0, 8},
    {0, 1, 8},
    {0, -1, 8}
};

int Piece::queen_opts[8][3] = {
    {1, 1, 8},
    {1, -1, 8},
    {-1, 1, 8},
    {-1, -1, 8},
    {1, 0, 8},
    {-1, 0, 8},
    {0, 1, 8},
    {0, -1, 8},
};

int Piece::king_opts[8][3] = {
    {-1, -1, 1},
    {-1,  0, 1},
    {-1,  1, 1},
    { 0, -1, 1},
    { 0,  1, 1},
    { 1, -1, 1},
    { 1,  0, 1},
    { 1,  1, 1}
};

int Piece::knight_opts[8][3] = {
    {-2,  1, 1},
    {-1,  2, 1},
    { 1,  2, 1},
    { 2,  1, 1},
    { 2, -1, 1},
    { 1, -2, 1},
    {-1, -2, 1},
    {-2, -1, 1}
};
