#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cstdlib>

#include "move.h"
#include "piece.h"

#define tab "\t"

using namespace std;

Move::Move():
        piece(Piece::EMPTY)
{
    start_row = 0;
    start_col = 0;
    end_row = 0;
    end_col = 0;
}

Move::Move(Piece p, pair<int, int> s, pair<int, int> e):
        piece(p)
{
    start_row = s.first;
    start_col = s.second;
    end_row = e.first;
    end_col = e.second;
}

Move::Move(Piece p, int start_row, int start_col, int end_row, int end_col) :
        piece(p),
        start_row(start_row),
        start_col(start_col),
        end_row(end_row),
        end_col(end_col)
{

}

void Move::print(bool uci) const {
    cerr << toString(uci) << endl;
}
    
string Move::toString(bool uci) const {
    if (uci) {
        char str[6];
        str[0] = start_col + 'a';
        str[1] = start_row + '1';
        str[2] = end_col + 'a';
        str[3] = end_row + '1';
        str[4] = '\0';
        str[5] = '\0';
        if (!get_promotion_piece().equals(Piece::EMPTY)) {
            switch (promotion_piece) {
                case Piece::WHITE_QUEEN:
                case Piece::BLACK_QUEEN:
                    str[4] = 'q';
                    break;

                case Piece::WHITE_ROOK:
                case Piece::BLACK_ROOK:
                    str[4] = 'r';
                    break;

                case Piece::WHITE_BISHOP:
                case Piece::BLACK_BISHOP:
                    str[4] = 'b';
                    break;

                case Piece::WHITE_KNIGHT:
                case Piece::BLACK_KNIGHT:
                    str[4] = 'n';
                    break;

                default:
                    cerr << "Cannot promote to something that isn't "
                         << "{ Queen, Rook, Bishop, Knight }" << endl;
                    cerr << "Tried to promote to "
                         << promotion_piece.get_piece_string() << endl;
                    std::terminate();
            }
        }

        string cpp_str(str);
        return cpp_str;
    } else {
        char str[9];
        str[0] = start_col + 'a';
        str[1] = start_row + '1';
        str[2] = ' ';
        str[3] = '-';
        str[4] = '>';
        str[5] = ' ';
        str[6] = end_col + 'a';
        str[7] = end_row + '1';
        str[8] = '\0';
        string cpp_str(str);
        return cpp_str;
    }
}
