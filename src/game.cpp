#include "game.h"

#include "board.h"

#include <chrono>
#include <cassert>


#define POSITIONS_PER_SECOND 200000
#define ENDGAME_SLOWDOWN 2

using namespace std;
using namespace std::chrono;

Game::Game(Options& options) :
  gf(GameFile(options)),
  book(OpeningBook("opening_book/")),
  options(options) {
  current_position = make_shared<Position>(gf.get_board(), gf.is_white());
  }

void Game::play() {
    //  current_position->make_move(1); // Need to generate a layer of tree

  if (!gf.is_white()) {
      //    current_position->make_move(1);
    blackMove();
  }

  while(true) {
    whiteMove();
    blackMove();
  }
}


void Game::whiteMove() {
  if (options.our_colour == Colour::WHITE) {
    myMove();
  } else {
    theirMove();
  }
}

void Game::blackMove() {
  if (options.our_colour == Colour::BLACK) {
    myMove();
  } else {
    theirMove();
  }
}


void Game::move(Move& m, bool print) {
  assert(current_position != nullptr);

  if (print) {
    // Print to console
    cerr << "(cpp) Move: " << m.toString(options.uci) << endl;

    // This should be cout.
    cout << m.toString(options.uci) << endl;
  }

  gf.execute_move(m);
  gf.write_move(m);


  current_position = current_position->move_into_tree(m);

  cerr << "(cpp) Move into tree done" << endl;

  if (current_position->is_checkmate()) {
    cerr << "(cpp) Checkmate" << endl;
    cerr << "(cpp) Exiting" << endl;
    exit(0);
  }
}

void Game::myMove() {
  Board board = gf.get_board();
  time_point<system_clock> start, end;

  if (!options.quiet) {
    board.print();
  }

  if (in_opening_book && book.in_opening_book(current_position->b)) {
    cerr << "Opening Book" << endl;

    Move m = book.next_move(current_position->b);

    move(m, true);

    current_position->make_move(1); // Need to generate a layer of tree
  } else {
    if (in_opening_book) {
      cerr << "Out of opening book" << endl;
      if (options.quiet) {
        // If !quiet we print it anyway.
        board.print();
      }
      in_opening_book = false;
    }

    Move m;

    assert(current_position != nullptr);

    if (current_position->is_end_game()) {
      cerr << "End game :-)" << endl;
    }

    start = system_clock::now();
    current_position->make_move(4);
    end = system_clock::now();

    if (!options.quiet) {
      //current_position->print_debug_tree();
    }

    duration<double> elapsed_seconds = (end - start);
    cerr << "(cpp) Move Time: " << elapsed_seconds.count() << endl;

    if (!options.quiet) {
      current_position->print_move_chain();
    }

    if (current_position->is_checkmate()) {
      cerr << "Checkmate" << endl;
      exit(0);
    }
    Move best_move = current_position->best_child()->m;

    move(best_move, true);

    cerr << "(cpp) Moved." << endl;
  }

  if (options.is_test) {
    exit(0);
  }
}

void Game::theirMove() {
  string op_move;

  if (!options.quiet) {
    cerr << "Please enter the opponent's move, and press return." << endl;
    cerr << "A blank line exits the program." << endl;
  } else {
    cerr << "(cpp) Waiting for opponent's move" << endl;
  }

  getline(cin, op_move);

  cerr << "(cpp) Opponent Move: " << op_move << endl;

  if (op_move.length() == 0) {
    exit(0);
  } else {
    Move m = gf.parse_move(op_move, options.uci);
    move(m, false);
  }
}


