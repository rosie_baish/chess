// -*- c++ -*-

#pragma once

#include "opening_book.h"
#include "game_file.h"
#include "options.h"
#include "position.h"
#include "move.h"

using namespace std;

class Game {
private:
    shared_ptr<Position> current_position;
    GameFile gf;
    OpeningBook book;
    Options& options;

    bool in_opening_book = true;

    void move(Move& m, bool print);

    void whiteMove();
    void blackMove();
    void theirMove();
    void myMove();
    
public:
    Game(Options& options);

    void play();

};
