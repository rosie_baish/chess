#include "game_file.h"

#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

using namespace std;

GameFile::GameFile(Options options) :
        filename(options.game_file),
        is_test(options.is_test),
        new_game(options.new_game)
{
    if (new_game) {
        ifstream f(filename);
        if (f.good()) {
            cerr << "Cannot create new file - already exists" << endl;
            exit(-1);
        }
        
        ofstream outfile (filename);
        outfile << "start" << endl;
        outfile.close();
    }

    string boardType;
    list<string> lines;
    string line;
    ifstream file(filename);
    if (file.is_open()) {
        getline(file, boardType);
        while ( getline (file,line) )
        {
            lines.push_back(line);
        }
        file.close();
    } else {
        cerr << "Unable to open file " << filename << endl;
        exit(-1);
    }

    if (boardType.compare("blank") == 0) {
        is_puzzle = true;
        board = Board::setup_blank_board();
        for (string line : lines) {
            if (line == "") {
                continue;
            }
            if (line.at(0) == '#') {
                // comment;
                if (line.substr(2, 5) == "white") {
                    _is_white = true;
                } else if (line.substr(2, 5) == "black") {
                    _is_white = false;
                }

                continue;
            }
            Piece piece(line.substr(0, 2));
            int col = line.at(3) - 'a';
            int row = line.at(4) - '1';

            if (col < 0 || col >= 8 || row < 0 || row >= 8) {
                cerr << "Invalid position: " << line.substr(3, 5) << endl;
                exit(-1);
            }

            board.set_piece(row, col, piece);
        }
    } else if (boardType.compare("start") == 0) {
        is_puzzle = false;
        board = Board::setup_board();

        for (string line : lines) {
            if (line.length() == 0) {
                continue;
            }
            
            execute_move(line, true); // Files use uci notation
        }
    } else {
        cerr << "Error: " << boardType << " is not a valid board type." << endl;
        cerr << "Options are \"blank\" or \"start\"." << endl;
        exit(-1);
    }
}

Move GameFile::parse_move(string line, bool uci) {
    int from_col, from_row, to_col, to_row;
    if (!uci) {
        from_col = line.at(0) - 'a';
        from_row = line.at(1) - '1';
        to_col = line.at(6) - 'a';
        to_row = line.at(7) - '1';

        if (from_col < 0 || from_col > 7 || from_row < 0 || from_row > 7) {
            cerr << "Invalid position: " << line.substr(0, 2) << endl;
            exit(-1);
        }
        if (to_col < 0 || to_col > 7 || to_row < 0 || to_row > 7) {
            cerr << "Invalid position: " << line.substr(6, 8) << endl;
            exit(-1);
        }
    } else {
        // UCI notation
        from_col = line.at(0) - 'a';
        from_row = line.at(1) - '1';
        to_col = line.at(2) - 'a';
        to_row = line.at(3) - '1';

        if (from_col < 0 || from_col > 7 || from_row < 0 || from_row > 7) {
            cerr << "Invalid position: " << line.substr(0, 2) << endl;
            exit(-1);
        }
        if (to_col < 0 || to_col > 7 || to_row < 0 || to_row > 7) {
            cerr << "Invalid position: " << line.substr(6, 8) << endl;
            exit(-1);
        }
    }

    Piece p(board.get_piece(from_row, from_col));
    Move m(p, from_row, from_col, to_row, to_col);

    // Check for promotion.
    // Only for uci
    if (uci && line.length() == 5) {
        switch(line.at(4)) {
            case 'q':
                m.set_promotion_piece(p.is_white()
                                      ? Piece::WHITE_QUEEN
                                      : Piece::BLACK_QUEEN);
                break;
            case 'r':
                m.set_promotion_piece(p.is_white()
                                      ? Piece::WHITE_ROOK
                                      : Piece::BLACK_ROOK);
                break;
            case 'b':
                m.set_promotion_piece(p.is_white()
                                      ? Piece::WHITE_BISHOP
                                      : Piece::BLACK_BISHOP);
                break;
            case 'n':
                m.set_promotion_piece(p.is_white()
                                      ? Piece::WHITE_KNIGHT
                                      : Piece::BLACK_KNIGHT);
                break;
            default:
                cerr << "Error - promotion piece was not one of {q, r, b, n}"
                     << endl;
                cerr << "Was '" << line.at(4) << "'" << endl;
                exit(-1);
        }
    }

    return m;
}

void GameFile::execute_move(string line, bool uci) {
    Move m = parse_move(line, uci);
    execute_move(m);
}

void GameFile::execute_move(Move m) {
    board = board.execute_move(m);
    _is_white = !_is_white;
}

Board GameFile::get_board() {
    return board;
}

void GameFile::write_move(string move) {
    assert(move != "");

    if (!(is_test || is_puzzle)) {
        ofstream outfile;

        outfile.open(filename, ios_base::app);

        outfile << move;

        outfile.close();
    }
}

void GameFile::write_move(const Move& m) {
    write_move(m.toString(true) + "\n");
}
