#include "game.h"
#include "options.h"

int main(int argc, char* argv[]) {
    Options options(argc, argv);

    Game g(options);

    g.play();

    return 0;
}
