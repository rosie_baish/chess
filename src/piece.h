// -*- c++ -*-
#pragma once

#include <string>
#include <list>
#include <map>

using namespace std;

class Piece {
  public:
    enum Value : uint8_t {
        EMPTY = 0,
        BLACK_ROOK = 1,
        BLACK_KNIGHT = 2,
        BLACK_BISHOP = 3,
        BLACK_KING = 4,
        BLACK_QUEEN = 5,
        BLACK_PAWN = 6,

        WHITE_ROOK = 9,
        WHITE_KNIGHT = 10,
        WHITE_BISHOP = 11,
        WHITE_KING = 12,
        WHITE_QUEEN = 13,
        WHITE_PAWN = 14
    };

    Piece() = default;
    constexpr Piece(Value piece_val) : value(piece_val) { };
    explicit Piece(int piece_val) : value((Value) piece_val) { };
    Piece(string piece_val);

    operator Value() const { return value; }
    
    std::string get_piece_string() const;

    bool equals(Piece p);

    bool is_black() const;
    bool is_white() const;
    bool is_pawn() const;
    bool is_knight() const;
    bool is_empty() const;
    bool is_black_king() const;
    bool is_white_king() const;

    bool same_colour(const Piece& p) const;
    bool diff_colour(const Piece& p) const;
    
    int max_moves() const;

    int (*move_options() const)[3];

  private:
    Value value;

    static map<string, Value> string_lookup;
    static void fill_lookup();
    static bool lookup_filled;
    static int black_pawn_opts[3][3];
    static int white_pawn_opts[3][3];
    static int bishop_opts[4][3];
    static int rook_opts[4][3];
    static int queen_opts[8][3];
    static int king_opts[8][3];
    static int knight_opts[8][3];
};

inline bool Piece::equals(Piece p) {
    return (value == p.value);
}

inline bool Piece::is_black() const {
    switch(value) {
        case (BLACK_ROOK):
        case (BLACK_KNIGHT):
        case (BLACK_BISHOP):
        case (BLACK_KING):
        case (BLACK_QUEEN):
        case (BLACK_PAWN):
            return true;
        default:
            return false;
    }
}

inline bool Piece::is_white() const {
    switch(value) {
        case (WHITE_ROOK):
        case (WHITE_KNIGHT):
        case (WHITE_BISHOP):
        case (WHITE_KING):
        case (WHITE_QUEEN):
        case (WHITE_PAWN):
            return true;
        default:
            return false;
    }
}

inline bool Piece::is_pawn() const {
    return (value == WHITE_PAWN || value == BLACK_PAWN);
}

inline bool Piece::is_knight() const {
    return (value == WHITE_KNIGHT || value == BLACK_KNIGHT);
}

inline bool Piece::is_empty() const {
    return value == EMPTY;
}

inline bool Piece::is_black_king() const {
    return value == BLACK_KING;
}
inline bool Piece::is_white_king() const {
    return value == WHITE_KING;
}


inline bool Piece::same_colour(const Piece& p) const {
    return ((is_white() && p.is_white()) ||
            (is_black() && p.is_black()));
}

inline bool Piece::diff_colour(const Piece& p) const {
    return ((is_black() && p.is_white()) ||
            (is_white() && p.is_black()));
}

inline int Piece::max_moves() const {
    switch (value) {
        case (BLACK_PAWN):
        case (WHITE_PAWN):
            return  3; // forward, plus 2 takes
            
        case (BLACK_BISHOP):
        case (WHITE_BISHOP):
        case (BLACK_ROOK):
        case (WHITE_ROOK):
            return 4;
        break;

        case (BLACK_QUEEN):
        case (WHITE_QUEEN):
        case (BLACK_KING):
        case (WHITE_KING):
        case (BLACK_KNIGHT):
        case (WHITE_KNIGHT):
            return 8;
        break;

        case (EMPTY):
        default:
            return 0;
    }
}


inline int (*Piece::move_options() const)[3] {
    switch (value) {
        case (BLACK_PAWN):
            return black_pawn_opts;

        case (WHITE_PAWN):
            return white_pawn_opts;      
        
        case (BLACK_BISHOP):
        case (WHITE_BISHOP):
            return bishop_opts;      
        
        case (BLACK_ROOK):
        case (WHITE_ROOK): 
            return rook_opts;      
        
        case (BLACK_QUEEN):
        case (WHITE_QUEEN): 
            return queen_opts;      

        case (BLACK_KING):
        case (WHITE_KING): 
            return king_opts;      
      
        case (BLACK_KNIGHT):
        case (WHITE_KNIGHT):
            return knight_opts;

        case (EMPTY):
        default:
            return 0;
    }
}
