#include "options.h"

#include <iostream>

void Options::print_help_and_terminate() {
    string tab = "\t";
    cerr << "Usage: " << program_name << " [options] [filename]" << endl;
    cerr << "Options:" << endl;
    cerr << tab << "--help" << tab << tab << "Print this help message." << endl;
    cerr << tab << "--new-game" << tab << "Create a new game in the specified file." << endl;
    cerr << tab << "--quiet" << tab << tab << "Only output move, nothing else." << endl;
    cerr << tab << "--test" << tab << tab << "Do not write anything to the gamefile." << endl;
    cerr << tab << tab << tab << "Used to repeatedly run a position." << endl;
    exit(-1);
}

Options::Options(int argc, char* argv[]) {
    program_name = argv[0];

    if (argc < 2) {
        print_help_and_terminate();
    }


    for (int i = 1; i < argc - 1; i++) {
        if (!(argv[i][0] == '-' && argv[i][1] == '-')) {
            print_help_and_terminate();
        }

        string arg(argv[i]);

        if (arg.compare("--test") == 0) {
            is_test = true;
        } else if (arg.compare("--new-game") == 0) {
            new_game = true;
        } else if (arg.compare("--help") == 0) {
            print_help_and_terminate();
        } else if (arg.compare("--quiet") == 0) {
            quiet = true;
        } else if (arg.compare("--move-time") == 0) {
            if (i + 1 >= argc - 1) {
                print_help_and_terminate();
            }
            string secs_string = argv[i+1];
            secs_per_move = stoi(secs_string);
            i++;
        } else if (arg.compare("--uci") == 0) {
            uci = true;
        } else if (arg.compare("--white") == 0) {
            our_colour = Colour::WHITE;
        } else if (arg.compare("--black") == 0) {
            our_colour = Colour::BLACK;
        } else {
            cerr << "Unrecogised argument: " << arg << endl;
            print_help_and_terminate();
        }
    }

    game_file = string(argv[argc - 1]);

    if (game_file.compare("--help") == 0) {
        print_help_and_terminate();
    }

    //    int secs_per_move = MAX_SECONDS_PER_MOVE;

}
