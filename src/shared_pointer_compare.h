#pragma once

/*
  This class means that any 2 pointers will be compared using their base
  comparison function.
  To use, put "PointerCompare" as the third template argument of a STL container
  of pointers.
  e.g. priority_queue<Position*, vector<Position*>, PointerCompare>
*/
class SharedPointerCompare {
public:
  template<typename T>
  bool operator()(shared_ptr<T> a, shared_ptr<T> b) {
    return (*a) < (*b);
  }
};
