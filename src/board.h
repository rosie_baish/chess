#pragma once

#include <list>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include "move.h"
#include "piece.h"

class Board {
public:
    Piece board[64];
    static Board setup_board();
    static Board setup_blank_board();

    bool operator == (const Board& other) const;

    Piece get_piece(int row, int col) const;
    void set_piece(pair<int, int> square, Piece piece);
    void set_piece(int row, int col, Piece piece);
    void set_piece_char(char col, int row, Piece piece);

    Board execute_move(const Move& move) const;

    void print() const;
};


inline Piece Board::get_piece(int row, int col) const {
    return board[8 * row + col];
}
