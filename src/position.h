// -*- c++ -*-

#pragma once

#include <limits.h>
#include <queue>
#include <vector>
#include <list>
#include <memory>

#include "move.h"
#include "board.h"
#include "shared_pointer_compare.h"

#define MAX_HEURISTIC ((INT_MAX - 1000))
#define MIN_HEURISTIC ((INT_MIN + 1000))

#define PAWN_POINTS 100

using namespace std;

class Position {
public:
    Board b;
    Move m;
    
private:
    // Member Variables
    bool isWhite;
    bool isLeaf = true;
    bool childrenGenerated = false;
    bool existValidChildren = false;

    vector<shared_ptr<Position>> children;
    priority_queue<shared_ptr<Position>,
                   vector<shared_ptr<Position>>,
                   SharedPointerCompare> ordered_children;

    // Note - move list includes things that would move you into check.
    // King cannot move into any square on opposing move list.
    // TODO - sort out kings taking opposing pieces which are covered
    vector<Move*> white_moves;
    vector<Move*> black_moves;

    bool isCheckmate = false;
    int heuristic;
    bool heuristic_not_calc = true;
    int moves_into_game = 0;
    
    int white_cover[64];
    int black_cover[64];

private:
    // Private Functions
    void add_white_move(Move* m);
    void add_black_move(Move* m);
    
    void create_move_lists();
    void add_move_to_list_inc_proms(Piece& p, Move& m);
    void add_move_to_list(Piece& p, Move& m);
    
    friend bool operator<(const Position& lhs, const Position& rhs);

    Board execute_move(Move m);

    // these are in heuristic.cpp
    int calculate_board_heuristic();
    void calculate_heuristic();

    void expand(int new_depth, int alpha, int beta);
    void print_debug_tree_internal(int tabs);

public:
    // Public Functions
    Position(); // Start of the game
    Position(Board b, bool isWhite); // Creating a puzzle

    Position(Position* p, Move m);
    ~Position();

    // Remove copy constructors
    Position(const Position&) = delete;
    void operator=(const Position&) = delete;

    void make_move(int depth);
    void generate_children();
    void check_for_valid_children();
    void add_white_move(int row, int col, Piece& p);
    void add_black_move(int row, int col, Piece& p);

    shared_ptr<Position> move_into_tree(Move m);

    void delete_unneeded_nodes();

    // Board Manipulation Functions
    void add_to_covers(Move& m);

    void blank_white_cover();
    int get_white_cover(int row, int col) const;
    int get_white_cover(pair<int, int> pos) const;
    void blank_black_cover();
    int get_black_cover(int row, int col) const;
    int get_black_cover(pair<int, int> pos) const;



    // Getters
    Move get_move();
    shared_ptr<Position> best_child();
    bool found_checkmate();
    bool is_checkmate() { return isCheckmate; }

    // Print functions
    void print_move_options();
    void print_debug_tree();
    void print_move_chain();

    bool is_valid_move(const Move& m) const;

    pair<int, int> black_king_pos() const;
    bool black_in_check();
    int black_king_moves() const;
    pair<int, int> white_king_pos() const;
    bool white_in_check();
    int white_king_moves() const;

    bool is_end_game() const;
};
