#include <string>
#include <iostream>
#include <cassert>

#include "position.h"
#include "piece.h"

Board Board::setup_board() {
    Board b;
    b.set_piece_char('a', 1, Piece::WHITE_ROOK);
    b.set_piece_char('b', 1, Piece::WHITE_KNIGHT);
    b.set_piece_char('c', 1, Piece::WHITE_BISHOP);
    b.set_piece_char('d', 1, Piece::WHITE_QUEEN);
    b.set_piece_char('e', 1, Piece::WHITE_KING);
    b.set_piece_char('f', 1, Piece::WHITE_BISHOP);
    b.set_piece_char('g', 1, Piece::WHITE_KNIGHT);
    b.set_piece_char('h', 1, Piece::WHITE_ROOK);

    for (int i = 'a'; i <= 'h'; i++) {
        b.set_piece_char(i, 2, Piece::WHITE_PAWN);
        b.set_piece_char(i, 3, Piece::EMPTY);
        b.set_piece_char(i, 4, Piece::EMPTY);
        b.set_piece_char(i, 5, Piece::EMPTY);
        b.set_piece_char(i, 6, Piece::EMPTY);
        b.set_piece_char(i, 7, Piece::BLACK_PAWN);
    }
    
    b.set_piece_char('a', 8, Piece::BLACK_ROOK);
    b.set_piece_char('b', 8, Piece::BLACK_KNIGHT);
    b.set_piece_char('c', 8, Piece::BLACK_BISHOP);
    b.set_piece_char('d', 8, Piece::BLACK_QUEEN);
    b.set_piece_char('e', 8, Piece::BLACK_KING);
    b.set_piece_char('f', 8, Piece::BLACK_BISHOP);
    b.set_piece_char('g', 8, Piece::BLACK_KNIGHT);
    b.set_piece_char('h', 8, Piece::BLACK_ROOK);

    return b;
}

Board Board::setup_blank_board() {
    Board b;
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            b.set_piece(row, col, Piece::EMPTY);
        }
    }
    
    return b;
}



bool Board::operator == (const Board& other) const {
    for (int i = 0; i < 64; i++) {
        if (board[i] != other.board[i]) {
            return false;
        }
    }
    return true;
}

void Board::set_piece(pair<int, int> square, Piece piece) {
    set_piece(square.first, square.second, piece);
}

void Board::set_piece(int row, int col, Piece piece) {
    board[8 * row + col] = piece;
}

void Board::set_piece_char(char col, int row, Piece piece) {
    set_piece(row - 1, col - 'a', piece);
}




Board Board::execute_move(const Move& move) const {
    Board new_b = setup_blank_board();
    for (int i = 0; i < 64; i++) {
        new_b.board[i] = board[i];
    }

    int from_col = move.get_start_col();
    int from_row = move.get_start_row();
    int to_col = move.get_end_col();
    int to_row = move.get_end_row();
    Piece piece = move.get_piece();

    Move castle_WK(Piece::WHITE_KING, 0, 4, 0, 6);
    Move castle_WQ(Piece::WHITE_KING, 0, 4, 0, 2);
    Move castle_BK(Piece::BLACK_KING, 7, 4, 7, 6);
    Move castle_BQ(Piece::BLACK_KING, 7, 4, 7, 2);

    assert(!piece.is_empty());

    // Promotion and En Passent
    if (piece == Piece::BLACK_PAWN || piece == Piece::WHITE_PAWN) {
        if (piece.equals(Piece::WHITE_PAWN) && to_row == 7) {
            // promotion
            new_b.set_piece(from_row, from_col, Piece::EMPTY);
            Piece pp = move.get_promotion_piece();
            assert(pp.equals(Piece::WHITE_QUEEN) ||
                   pp.equals(Piece::WHITE_ROOK) ||
                   pp.equals(Piece::WHITE_KNIGHT) ||
                   pp.equals(Piece::WHITE_BISHOP));
            if (pp.is_empty()) {
                cerr << "Error - promotion piece not set" << endl;
                std::terminate();
            }
            new_b.set_piece(to_row, to_col, pp);
        } else if (piece.equals(Piece::BLACK_PAWN) && to_row == 0) {
            // promotion
            new_b.set_piece(from_row, from_col, Piece::EMPTY);
            Piece pp = move.get_promotion_piece();
            assert(pp.equals(Piece::BLACK_QUEEN) ||
                   pp.equals(Piece::BLACK_ROOK) ||
                   pp.equals(Piece::BLACK_KNIGHT) ||
                   pp.equals(Piece::BLACK_BISHOP));
            if (pp.is_empty()) {
                cerr << "Error - promotion piece not set" << endl;
                std::terminate();
            }
            new_b.set_piece(to_row, to_col, pp);
        } else if ((to_col != from_col) &&
                   new_b.get_piece(to_row, to_col).equals(Piece::EMPTY)) {
            // En Passent
            if (to_row == 2) {
                new_b.set_piece(3, to_col, Piece::EMPTY);
            }

            if (to_row == 5) {
                new_b.set_piece(4, to_col, Piece::EMPTY);
            }

            // Need to actually move the pawn
            new_b.set_piece(from_row, from_col, Piece::EMPTY);
            new_b.set_piece(to_row, to_col, piece);
        } else {
            new_b.set_piece(from_row, from_col, Piece::EMPTY);
            new_b.set_piece(to_row, to_col, piece);
        }
        return new_b;
    }

    new_b.set_piece(from_row, from_col, Piece::EMPTY);
    new_b.set_piece(to_row, to_col, piece);

    // Check for castling. If so, do the rook move as well.
    if (move == castle_WK) {
        new_b.set_piece(0, 7, Piece::EMPTY);
        new_b.set_piece(0, 5, Piece::WHITE_ROOK);
    } else if (move == castle_WQ) {
        new_b.set_piece(0, 0, Piece::EMPTY);
        new_b.set_piece(0, 3, Piece::WHITE_ROOK);
    } else if (move == castle_BK) {
        new_b.set_piece(7, 7, Piece::EMPTY);
        new_b.set_piece(7, 5, Piece::BLACK_ROOK);
    } else if (move == castle_BQ) {
        new_b.set_piece(7, 0, Piece::EMPTY);
        new_b.set_piece(7, 3, Piece::BLACK_ROOK);
    }

    return new_b;
}

void Board::print() const {
    for (int i = 7; i >= 0; i--) {
        for (int j = 0; j < 8; j++) {
            std::cerr << "+--";
        }
        std::cerr << "+\n";

        for (int j = 0; j < 8; j++) {
            Piece p(get_piece(i, j));
            std::cerr << "|" << p.get_piece_string();
        }
        std::cerr << "|\n";
    }



    for (int i = 0; i < 8; i++) { 
        std::cerr << "+--";
    }
    std::cerr << "+\n\n";
    return;
}
