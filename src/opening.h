#pragma once

#include "board.h"
#include "move.h"

#include <string>
#include <list>

using namespace std;

class Opening {
private:    
    string name;
    list<pair<Board, Move>> sequence;
    Board generate_board(Board b, string move_line);
    Move generate_next_move(Board b, string move_line);
    
    Move create_move(Board board, string line);
    
    
public:
    Opening(string filename);
    list<pair<Board, Move>> get_sequence();
    string get_name() { return name; }
};
