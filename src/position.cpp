#include "position.h"

#include <iostream>
#include <cassert>
#include <algorithm>

//#define DEBUG_1
//#define DEBUG_2

#ifdef DEBUG_1
#define DEBUG true
#define PRINT_DEBUG true
#else
#define DEBUG false
#define PRINT_DEBUG false
#endif

#ifdef DEBUG_2
#define PRINT_MORE_DEBUG true
#else
#define PRINT_MORE_DEBUG false
#endif

using namespace std;

string tab = "\t";

#define OnBoard(row, col) ((row) >= 0 && (row) < 8 && (col) >= 0 && (col) < 8)

bool white_move_order (Move* a, Move* b) {
    return (a->get_piece() > b->get_piece());
}

bool black_move_order (Move* a, Move* b) {
    return (a->get_piece() < b->get_piece());
}


Position::Position() {
    // Initial position at the start of the game.
    b = Board::setup_board();
    isWhite = true;
    create_move_lists();
}

Position::Position(Board b, bool isWhite) :
        b(b),
        isWhite(isWhite) {
    create_move_lists();
}

Position::Position(Position* p, Move m) :
        m(m) {
    b = p->b.execute_move(m);
    isWhite = !(p->isWhite);
    moves_into_game = p->moves_into_game + 1;
    create_move_lists();
}

Position::~Position() {
    for (Move* m : white_moves) {
        assert(m != nullptr);
        delete m;
    }
    for (Move* m : black_moves) {
        assert(m != nullptr);
        delete m;
    }
}

/*
  Private Functions
*/
void Position::add_to_covers(Move& m) {
    assert((m.get_piece().is_black() || m.get_piece().is_white()) && "Cannot be empty square");

    if (m.get_piece().is_white()) {
        white_cover[8 * m.get_end_row() + m.get_end_col()]++;
    } else {
        black_cover[8 * m.get_end_row() + m.get_end_col()]++;
    }
}

void Position::add_move_to_list_inc_proms(Piece& p, Move& m) {
    assert((p.is_black() || p.is_white()) && "Cannot be empty square");

    assert(is_valid_move(m));


    if (!p.is_pawn()) {
        add_move_to_list(p, m);
        return;
    }

    // Piece is a pawn
    if (m.get_end_row() == 7) {
        // White promotion
        m.set_promotion_piece(Piece::WHITE_QUEEN);
        white_moves.push_back(new Move(m));
        add_to_covers(m);

        m.set_promotion_piece(Piece::WHITE_ROOK);
        white_moves.push_back(new Move(m));
        add_to_covers(m);

        m.set_promotion_piece(Piece::WHITE_BISHOP);
        white_moves.push_back(new Move(m));
        add_to_covers(m);

        m.set_promotion_piece(Piece::WHITE_KNIGHT);
        white_moves.push_back(new Move(m));
        add_to_covers(m);
    } else if (m.get_end_row() == 0) {
        // Black promotion
        m.set_promotion_piece(Piece::BLACK_QUEEN);
        black_moves.push_back(new Move(m));
        add_to_covers(m);

        m.set_promotion_piece(Piece::BLACK_ROOK);
        black_moves.push_back(new Move(m));
        add_to_covers(m);

        m.set_promotion_piece(Piece::BLACK_BISHOP);
        black_moves.push_back(new Move(m));
        add_to_covers(m);

        m.set_promotion_piece(Piece::BLACK_KNIGHT);
        black_moves.push_back(new Move(m));
        add_to_covers(m);
    } else {
        // Normal pawn move
        add_move_to_list(p, m);
    }
}

void Position::add_move_to_list(Piece& p, Move& m) {
    assert((p.is_black() || p.is_white()) && "Cannot be empty square");

    add_to_covers(m);
    if (p.is_white()) {
        white_moves.push_back(new Move(m));
    } else {
        black_moves.push_back(new Move(m));
    }
}

void Position::create_move_lists() {
    blank_white_cover();
    blank_black_cover();

    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            Piece p(b.get_piece(row, col));
            // We get a group of n arrays, each of length 3.
            // n is the number of directions it can move.
            // array is { d_row, d_col, max_distance }
            int (*options)[3] = p.move_options();
            for (int i = 0; i < p.max_moves(); i++) {
                int d_row = options[i][0];
                int d_col = options[i][1];
                int max_dist = options[i][2];
                int new_row = row;
                int new_col = col;
                for (int dist = 1; dist <= max_dist; dist++) {
                    new_row += d_row;
                    new_col += d_col;
                    if (!OnBoard(new_row, new_col)) {
                        break;
                    }
                    Move move(p, row, col, new_row, new_col);
                    if (!is_valid_move(move)) {
                        break;
                    }
                    add_move_to_list_inc_proms(p, move);
                }
            }
        }
    }

    sort(white_moves.begin(), white_moves.end(), white_move_order);
    sort(black_moves.begin(), black_moves.end(), black_move_order);
}

void Position::expand(int depth, int alpha, int beta) {
    if (depth == 0) {
        check_for_valid_children();
        calculate_heuristic();
        return;
    }

    // All non-leaf nodes have children already.
    if (!childrenGenerated) {
        generate_children();
    }

    calculate_heuristic();

    if (isCheckmate) {
        // If this is checkmate we can't expand any further
        return;
    }

    if (depth < 0) {
        cerr << "Something went wrong." << endl;
        cerr << "Depth should not be negative" << endl;
        std::terminate();
    }

    if (DEBUG) {
        cerr << "Depth: " << depth << endl;
        cerr << "Heuristic: " << heuristic << endl;
        cerr << "isWhite: " << (isWhite ? "True" : "False") << endl;
        cerr << endl;
    }

    // Additional check to reduce search depth.
    // If we have made a suboptimal sequence of moves, and not put our opponent in
    // check, then don't bother checking what they can do, as it will not improve
    // the situation
    // Suboptimal defined as at least a pawn down.
    if (depth == 1) {
        if (isWhite && !white_in_check()) {
            if ((heuristic - PAWN_POINTS) > beta) {
                return;
            }
        } else if (!isWhite && !black_in_check()) {
            if ((heuristic + PAWN_POINTS) < alpha) {
                return;
            }
        }
    }

    assert(!children.empty());

    if (isWhite) {
        if (!children.empty()) {
            int max_h = INT_MIN;
            int value = INT_MIN;

            for (auto child: children) {
              child->expand(depth - 1, alpha, beta);

                ordered_children.push(child);

                value = max(value, child->heuristic);

                if (PRINT_DEBUG) {
                    if (PRINT_MORE_DEBUG) { child->b.print(); }
                    cerr << tab << "White: ";
                    cerr << (child->black_king_moves()) << tab;
                    cerr << child->heuristic << tab;
                    child->m.print(false);
                    if (depth > 2) {
                        cerr << "\n\n";
                    }
                }

                if (value > max_h) {
                    max_h = child->heuristic;
                    heuristic = max_h;
                    heuristic_not_calc = false;
                }

                // alpha = max(alpha, value);
                if (value > alpha) {
                    alpha = value;
                }
                if (alpha >= beta) {
                    return;
                }
            }
        }
    } else {
        if (!children.empty()) {
            int min_h = INT_MAX;
            int value = INT_MAX;

            for (auto child: children) {
              child->expand(depth - 1, alpha, beta);

                ordered_children.push(child);

                value = min(value, child->heuristic);

                if (PRINT_DEBUG) {
                    if (PRINT_MORE_DEBUG) { child->b.print(); }
                    cerr << tab << "Black: ";
                    cerr << (child->white_king_moves()) << tab;
                    cerr << child->heuristic << tab;
                    child->m.print(false);
                }


                if (value < min_h) {
                    min_h = child->heuristic;
                    heuristic = min_h;
                    heuristic_not_calc = false;
                }

                if (value < beta) {
                    beta = value;
                }

                if (beta <= alpha) {
                    return;
                }
            }
        }
    }

    if (PRINT_DEBUG && !children.empty()) {
        if (PRINT_MORE_DEBUG) { b.print(); }
        cerr << "Optimal Score: " <<  heuristic << tab;
        get_move().print(false);
        cerr << tab << tab << tab;
        best_child()->m.print(false);
        cerr << "\n\n";
    }
    
}

bool operator<(const Position& lhs, const Position& rhs) {
    if (lhs.isWhite) {
        return (lhs.heuristic > rhs.heuristic);
    } else {
        return (lhs.heuristic < rhs.heuristic);
    }
}

/*
  Public Functions
  n.b. calculate_board_heuristic and calculate_heuristic are in heuristic.cpp
*/

void Position::make_move(int depth) {
    expand(depth, INT_MIN, INT_MAX);
}

void Position::generate_children() {
    if (isWhite) {
        assert(white_moves.size() != 0);
        for (Move* m : white_moves) {
            add_white_move(m);
        }
    } else {
        assert(black_moves.size() != 0);
        for (Move* m : black_moves) {
            add_black_move(m);
        }
    }
    childrenGenerated = true;
    isLeaf = false;
}

void Position::check_for_valid_children() {
    if (isWhite) {
        assert(white_moves.size() != 0);
        for (Move* m : white_moves) {
            Position child(this, *m);
            if (!child.white_in_check()) {
                // Can't move into check
                existValidChildren = true;
                break;
            }
        }
    } else {
        assert(black_moves.size() != 0);
        for (Move* m : black_moves) {
            Position child(this, *m);
            if (!child.black_in_check()) {
                // Can't move into check
                existValidChildren = true;
                break;
            }
        }
    }
}

void Position::add_white_move(Move* m) {
    auto child = make_shared<Position>(this, *m);
    if (!child->white_in_check()) {
        children.push_back(child);
        existValidChildren = true;
    }
}

void Position::add_black_move(Move* m) {
    auto child = make_shared<Position>(this, *m);
    if (!child->black_in_check()) {
        children.push_back(child);
        existValidChildren = true;
    }
}

shared_ptr<Position> Position::move_into_tree(Move move) {
    shared_ptr<Position> r_pos = nullptr;
    for (auto child : children) {
        if (child->m == move) {
            r_pos = child;
        }
    }
    children.clear();

    if (r_pos == nullptr) {
        cerr << "Error - unexpected move" << endl;
        b.print();
        cerr << "Move was: ";
        move.print(true);
        cerr << "Expected move chain: " << endl;
        print_move_chain();
        r_pos = make_shared<Position>(this, move);
        r_pos->b.print();
    }

    return r_pos;
}

/*
  Getters
*/

Move Position::get_move() {
    return best_child()->m;
}

shared_ptr<Position> Position::best_child() {
    if (isCheckmate) {
        return nullptr;
    } else {
        return ordered_children.top();
    }
}

bool Position::found_checkmate() {
    if (isCheckmate) {
        return true;
    } else {
        if (!isLeaf) {
            return best_child()->found_checkmate();
        } else {
            return false;
        }
    }    
}

/*
  Print Functions
*/

void Position::print_move_options() {
    for (auto child: children) {
        cerr << (isWhite ? "White:" : "Black:") << tab << child->m.toString(false);
        cerr << tab << child->heuristic << endl;
    }
}

void Position::print_debug_tree() {
    print_debug_tree_internal(0);
}

void Position::print_debug_tree_internal(int tabs) {
    if (isLeaf) { return; }

    for (auto child : children) {
        for (int i = 0; i < tabs; i++) { cerr << tab; }
        cerr << (isWhite ? "White:" : "Black:") << tab;
        cerr << child->m.toString(false) << tab;
        cerr << (child->isLeaf ? "Leaf" : "Node") << tab;
        if (child->isCheckmate) {
            if (heuristic == 0) {
                cerr << "Stalemate" << endl;
            } else {
                cerr << "Checkmate" << endl;
            }
        } else {
            if (heuristic_not_calc) {
                cerr << "Unknown heuristic" << endl;
            } else {
                cerr << child->heuristic << endl;
            }
        }
        child->print_debug_tree_internal(tabs + 1);
    }
}

void Position::print_move_chain() {
    if (isLeaf) {
        cerr << "Heuristic: " << heuristic << endl;
        return;
    }
    if (isCheckmate) {
        cerr << "Checkmate" << endl;
    } else {
        cerr << (isWhite ? "White" : "Black" ) << "move:\t";

        get_move().print(false);

        best_child()->print_move_chain();
    }
}

/*
  Board related stuff
*/
bool Position::is_valid_move(const Move& m) const {
    // NOTE - Assumes that all previous moves in this direction were valid.
    // i.e. if the move is a1a8, it assumes that a1a2, a1a3, ..., a1a7 are all valid.
    Piece p = m.get_piece();
    if (p.is_pawn()) {
        Piece dest(b.get_piece(m.get_end_row(), m.get_end_col()));
        if(m.delta_col() != 0) {
            return (p.diff_colour(dest));
        }
        if (!dest.is_empty()) { return false; }

        if (p.equals(Piece::BLACK_PAWN)) {
            if (m.delta_row() == 2 && m.get_start().first != 6) {
                return false;
            }
        }

        if (p.equals(Piece::WHITE_PAWN)) {
            if (m.delta_row() == 2 && m.get_start().first != 1) {
                return false;
            }
        }

        return true;
    } else if (p.is_knight()) {
        Piece dest(b.get_piece(m.get_end_row(), m.get_end_col()));
        // Can't move to same colour
        // Can move to empty or opposite colour
        return !p.same_colour(dest);
    } else {
        Piece dest(b.get_piece(m.get_end_row(), m.get_end_col()));
        Piece one_before(
            b.get_piece(m.get_end_row() - m.row_step(),
                        m.get_end_col() - m.col_step()));

        // We know we can move to previous square.
        // But it might be that we took something there.
        // Note: If this is our first move in a direction
        // the previous piece is us.
        // Hence not checking it isn't us.
        if (p.diff_colour(one_before)) { return false; }

        // Can't move to same colour
        // Can move to empty or opposite colour
        return !p.same_colour(dest);
    }
}

pair<int, int> Position::black_king_pos() const {
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            Piece piece = b.get_piece(row, col);
            if (piece.equals(Piece::BLACK_KING)) {
                return make_pair(row, col);
            }
        }
    }
    cerr << "No black king on the board" << endl;
    exit(-1);
}

bool Position::black_in_check() {
    return get_white_cover(black_king_pos());
}

int Position::black_king_moves() const {
    int bk_moves = 0;
    assert(!isWhite);
    for (auto child : children) {
        if (child->m.get_piece().equals(Piece::BLACK_KING)) {
            bk_moves++;
        }
    }
    return bk_moves;
}

pair<int, int> Position::white_king_pos() const {
    for (int row = 0; row < 8; row++) {
        for (int col = 0; col < 8; col++) {
            Piece piece = b.get_piece(row, col);
            if (piece.equals(Piece::WHITE_KING)) {
                return make_pair(row, col);
            }
        }
    }
    cerr << "No white king on the board" << endl;
    exit(-1);
}

bool Position::white_in_check() {
    return get_black_cover(white_king_pos());
}

int Position::white_king_moves() const {
    int wk_moves = 0;
    assert(isWhite);
    for (auto child : children) {
        if (child->m.get_piece().equals(Piece::WHITE_KING)) {
            wk_moves++;
        }
    }
    return wk_moves;
}

bool Position::is_end_game() const {
    int black_count = 0;
    int white_count = 0;
    for (int i = 0; i < 64; i++) {
        Piece p(b.board[i]);
        if (p.is_white()) { white_count++; }
        if (p.is_black()) { black_count++; }
    }
    if (white_count == 1 || black_count == 1) { return true; }

    return false;
}

void Position::blank_white_cover() {
    for (int i = 0; i < 64; i++) {
        white_cover[i] = 0;
    }
}

int Position::get_white_cover(int row, int col) const {
    return white_cover[8 * row + col];
}

int Position::get_white_cover(pair<int, int> pos) const {
    return white_cover[8 * pos.first + pos.second];
}

void Position::blank_black_cover() {
    for (int i = 0; i < 64; i++) {
        black_cover[i] = false;
    }
}

int Position::get_black_cover(int row, int col) const {
    return black_cover[8 * row + col];
}

int Position::get_black_cover(pair<int, int> pos) const {
    return black_cover[8 * pos.first + pos.second];
}

// Currently unused.
void Position::delete_unneeded_nodes() {

#define MAX_NUM_BEST_CHILDREN 10

    int num_best_children = min((int)ordered_children.size(),
                                (int)MAX_NUM_BEST_CHILDREN);

    children.clear();

    for (int i = 0; i < num_best_children; i++) {
        children.push_back(ordered_children.top());
        ordered_children.pop();
    }

    // Create a new priority queue. Saves walking the old one to empty it.
    ordered_children = priority_queue<shared_ptr<Position>,
                                      vector<shared_ptr<Position>>,
                                      SharedPointerCompare>();

    for (auto p : children) {
        ordered_children.push(p);
    }
}
