#include "opening.h"

#include <iostream>
#include <fstream>

using namespace std;

Opening::Opening(string filename) {
    string line;
    ifstream file(filename);
    if (file.is_open()) {
        getline(file, name);
        Board current_board = Board::setup_board();
        while ( getline (file,line) )
        {
            /*
              Lines with 1 moves contain 8 characters + CR
              Lines with 2 moves contain more than 10 characters
              We always need to work out the next move.
              We only need a new board state if there are 2 moves.
            */
            Move next_move = generate_next_move(current_board, line.substr(0, 8));
            sequence.push_back(make_pair(current_board, next_move));
            current_board = generate_board(current_board, line.substr(0, 8));


            int len = line.length();
            if (len > 10) {
                next_move = generate_next_move(current_board, line.substr(9, 8));
                sequence.push_back(make_pair(current_board, next_move));
                current_board = generate_board(current_board, line.substr(9, 8));
            }
        }
        file.close();
    } else {
        cerr << "Unable to open file " << filename << endl;
        exit(-1);
    }
}

Board Opening::generate_board(Board b, string move) {
    Move m = create_move(b, move);
    Board new_board = b.execute_move(m);
    return new_board;
}

Move Opening::generate_next_move(Board b, string move_line) {
    return create_move(b, move_line.substr(0, 8));
}

Move Opening::create_move(Board board, string line) {
    int from_col = line.at(0) - 'a';
    int from_row = line.at(1) - '1';
    int to_col = line.at(6) - 'a';
    int to_row = line.at(7) - '1';

    if (from_col < 0 || from_col > 7 || from_row < 0 || from_row > 7) {
        cerr << "Opening: " << name << endl;
        cerr << "Invalid position: " << line.substr(0, 2) << endl;
        exit(-1);
    }
    if (to_col < 0 || to_col > 7 || to_row < 0 || to_row > 7) {
        cerr << "Opening: " << name << endl;
        cerr << "Invalid position: " << line.substr(6, 8) << endl;
        exit(-1);
    }
    Piece p(board.get_piece(from_row, from_col));
    Move m(p, from_row, from_col, to_row, to_col);

    return m;
}

list<pair<Board, Move>> Opening::get_sequence() {
    return sequence;
}
