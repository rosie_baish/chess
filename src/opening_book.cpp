#include "opening_book.h"

#include <string>
#include <iostream>
#include <dirent.h>
#include <sys/types.h>
#include <ctime>
#include <cstdlib>

using namespace std;

OpeningBook::OpeningBook(string book_directory) :
        directory(book_directory)
{
    struct dirent *entry;
    DIR *dir = opendir(directory.data());

    if (dir == NULL) {
        return;
    }
    while ((entry = readdir(dir)) != NULL) {
        string filename(entry->d_name);

        book.push_back(Opening(directory + filename));

    }
    closedir(dir);
}

bool OpeningBook::in_opening_book(Board b) {
    for (Opening o : book) {
        list<pair<Board, Move>> moves = o.get_sequence();
        for (pair<Board, Move> p : moves) {
            if (p.first == b) {
                return true;
            }
        }
    }
    return false;
}

Move OpeningBook::next_move(Board b) {
    list<pair<Opening, Move>> valid_moves;
    for (Opening o : book) {
        list<pair<Board, Move>> moves = o.get_sequence();
        for (pair<Board, Move> p : moves) {
            if (p.first == b) {
                valid_moves.push_back({o, p.second});
            }
        }
    }

    srand(time(0));

    int num_moves = valid_moves.size();

    int move_num = (rand() % num_moves);

    auto it = next(valid_moves.begin(), move_num);

    cerr << "Playing opening " << (*it).first.get_name() << endl;
    return (*it).second;
}
