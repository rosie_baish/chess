#pragma once

#include "board.h"
#include "opening.h"

#include <string>
#include <list>

using namespace std;

class OpeningBook {
private:
    string directory;
    list<Opening> book;
    
public:
    OpeningBook(string directory);
    bool in_opening_book(Board b);
    Move next_move(Board b);
};
