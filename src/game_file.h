#pragma once

#include <iostream>
#include "board.h"
#include "options.h"

using namespace std;

class GameFile {
private:
    string filename;
    bool is_test;
    bool new_game;
    bool is_puzzle;
    Board board;
    bool _is_white = true;

public:
    GameFile(Options options);

    Move parse_move(string line, bool uci);

    void execute_move(string line, bool uci);
    void execute_move(Move m);
    
    Board get_board();

    bool is_white() { return _is_white; }

    void write_move(string move);
    void write_move(const Move& m);
};
