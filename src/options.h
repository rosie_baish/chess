// -*- c++ -*-

#pragma once

#include <string>

#define MAX_SECONDS_PER_MOVE 30

using namespace std;

enum class Colour {
    WHITE,
    BLACK
};

class Options {
private:
    void print_help_and_terminate();

public:
    Options(int argc, char* argv[]);

    Colour our_colour = Colour::WHITE;
    bool is_test = false;
    bool new_game = false;
    bool quiet = false;
    bool uci = false;
    string program_name;
    string game_file;

    int secs_per_move = MAX_SECONDS_PER_MOVE;

};
