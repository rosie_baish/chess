// -*- c++ -*-
#pragma once

#include "piece.h"

using namespace std;

class Move {

private:
    Piece piece;
    Piece promotion_piece = Piece::EMPTY;
    uint8_t start_row;
    uint8_t start_col;
    uint8_t end_row;
    uint8_t end_col;

public:
    Move();
    Move(Piece piece, int start_row, int start_col, int end_row, int end_col);
    Move(Piece piece, pair<int, int> start, pair<int, int> end);

    bool operator == (const Move& other) const;

    bool valid_positions() const;

    int delta_row() const;
    int delta_col() const;
    int row_step() const;
    int col_step() const;

    int get_start_row() const;
    int get_start_col() const;
    int get_end_row() const;
    int get_end_col() const;
    pair<int, int> get_start() const;
    pair<int, int> get_end() const;

    void set_promotion_piece(Piece p) { promotion_piece = p; }

    Piece get_piece() const { return piece; }
    Piece get_promotion_piece() const { return promotion_piece; }

    void print(bool uci) const;

    string toString(bool uci) const;
};

inline bool Move::valid_positions() const {
    return ((start_row >= 0 && start_row < 8) &&
            (start_col >= 0 && start_col < 8) &&
            (end_row >= 0 && end_row < 8) &&
            (end_col >= 0 && end_col < 8));
}

inline int Move::delta_row() const {
    return abs(start_row - end_row);
}

inline int Move::delta_col() const {
    return abs(start_col - end_col);
}


inline int Move::row_step() const {
    if (end_row > start_row) {
        return 1;
    } else if (end_row == start_row) {
        return 0;
    } else {
        return -1;
    }
}


inline int Move::col_step() const {
    if (end_col > start_col) {
        return 1;
    } else if (end_col == start_col) {
        return 0;
    } else {
        return -1;
    }
}

inline int Move::get_start_row() const {
    return start_row;
}

inline int Move::get_start_col() const {
    return start_col;
}

inline int Move::get_end_row() const {
    return end_row;
}

inline int Move::get_end_col() const {
    return end_col;
}

inline bool Move::operator == (const Move& other) const {
    return ((piece == other.piece) &&
            (start_row == other.start_row) &&
            (start_col == other.start_col) &&
            (end_row == other.end_row) &&
            (end_col == other.end_col));
}

inline pair<int, int> Move::get_start() const {
    return make_pair(start_row, start_col);
}

inline pair<int, int> Move::get_end() const {
    return make_pair(end_row, end_col);
}
