#include "position.h"

#include <iostream>
#include <cassert>

using namespace std;

int is_black_piece(int piece) {
    switch(piece) {
        case (Piece::BLACK_KING):
        case (Piece::BLACK_QUEEN):
        case (Piece::BLACK_ROOK):
        case (Piece::BLACK_BISHOP):
        case (Piece::BLACK_KNIGHT):
        case (Piece::BLACK_PAWN):
            return 1;
        default:
            return 0;
    }
}

int piece_point(int piece) {
    switch(piece) {
        case (Piece::BLACK_KING):
            return -10000 * PAWN_POINTS;
        case (Piece::WHITE_KING):
            return 10000 * PAWN_POINTS;
            
        case (Piece::BLACK_QUEEN):
            return -9 * PAWN_POINTS;
        case (Piece::WHITE_QUEEN):
            return 9 * PAWN_POINTS;

        case (Piece::BLACK_ROOK):
            return -5 * PAWN_POINTS;
        case (Piece::WHITE_ROOK):
            return 5 * PAWN_POINTS;

        case (Piece::BLACK_BISHOP):
            return -3 * PAWN_POINTS;
        case (Piece::WHITE_BISHOP):
            return 3 * PAWN_POINTS;

        case (Piece::BLACK_KNIGHT):
            return -3 * PAWN_POINTS;
        case (Piece::WHITE_KNIGHT):
            return 3 * PAWN_POINTS;

        case (Piece::BLACK_PAWN):
            return -1 * PAWN_POINTS;
        case (Piece::WHITE_PAWN):
            return 1 * PAWN_POINTS;
    }
    return 0;
}

int square_multipliers[64] = {
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 2, 2, 2, 2, 2, 2, 1,
    1, 2, 3, 3, 3, 3, 2, 1,
    1, 2, 3, 4, 4, 3, 2, 1,
    1, 2, 3, 4, 4, 3, 2, 1,
    1, 2, 3, 3, 3, 3, 2, 1,
    1, 2, 2, 2, 2, 2, 2, 1,
    1, 1, 1, 1, 1, 1, 1, 1
};

int Position::calculate_board_heuristic() {
    int score = 0;

    for (int i = 0; i < 64; i++) {
        score += piece_point(b.board[i]);
        score += white_cover[i] * square_multipliers[i];
        score -= black_cover[i] * square_multipliers[i];
    }

    return score;

    // if we are in the end game, the use a special algorithm
    if (is_end_game()) {
        // In End Game

        // TODO: Rewrite this to allow use by black.
        score += 1000000;

        int pawn_distances = 0;
        int num_majors = 0;
        int num_minors = 0;

        // We want to encourage advancing pawns
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                switch(b.board[8 * row + col]) {
                    case Piece::WHITE_QUEEN:
                    case Piece::WHITE_ROOK:
                        num_majors++;
                        break;
                    case Piece::WHITE_KNIGHT:
                    case Piece::WHITE_BISHOP:
                        num_minors++;
                        break;
                    case Piece::WHITE_PAWN:
                        pawn_distances += row;
                        break;
                    default:
                        // do nothing
                        break;
                }
            }
        }

        if (num_majors == 0) {
            score += 1000 * pawn_distances;
        } else {
            score += 100 * pawn_distances;
        }


        bool can_go[8][8];
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                int p = b.get_piece(row, col);
                /*
                  NOTE (17 June 19)
                  I'm fairly certain this is flawed, because it should be negating
                  get_white_cover.
                */
                can_go[row][col] = get_white_cover(row, col) ||
                        (p != Piece::EMPTY && p != Piece::BLACK_KING);
            }
        }

        int king_row = black_king_pos().first;
        int king_col = black_king_pos().second;

        for (int i = 0; i < 8; i++) {
            bool full_row = true;
            bool full_col = true;
            for (int j = 0; j < 8; j++) {
                full_row = full_row && get_white_cover(i, j);
                full_col = full_col && get_white_cover(j, i);
            }

            if (full_row) {
                if (i < king_row) {
                    // fill below
                    for (int r = 0; r < i; r++) {
                        for (int c = 0; c < 8; c++) {
                            can_go[r][c] = false;
                        }
                    }
                } else if (i > king_row) {
                    // fill above
                    for (int r = 7; r > i; r--) {
                        for (int c = 0; c < 8; c++) {
                            can_go[r][c] = false;
                        }
                    }
                }
            }
            if (full_col) {
                if (i < king_col) {
                    // fill below
                    for (int c = 0; c < i; c++) {
                        for (int r = 0; r < 8; r++) {
                            can_go[r][c] = false;
                        }
                    }
                } else if (i > king_col) {
                    // fill above
                    for (int c = 7; c > i; c--) {
                        for (int r = 0; r < 8; r++) {
                            can_go[r][c] = false;
                        }
                    }
                }                    
            }
        }

        score += 10000;
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (can_go[row][col]) {
                    score -= 100;
                }
            }
        }

        score += 1000 - 100 * black_king_moves();

        pair<int, int> bk_pos = black_king_pos();
        pair<int, int> wk_pos = white_king_pos();

        int d_row = abs(bk_pos.first - wk_pos.first) + 1;
        int d_col = abs(bk_pos.second - wk_pos.second) + 1;

        score -= (d_row * d_row) + (d_col * d_col);

        return score;
    }
}

void Position::calculate_heuristic() {
    if (!existValidChildren) {
        isCheckmate = true;
        if (black_in_check()) {
            heuristic = MAX_HEURISTIC - moves_into_game;
        } else if (white_in_check()) {
            heuristic = MIN_HEURISTIC + moves_into_game;
        } else {
            // Stalemate
            heuristic = 0;
        }
    } else {
        isCheckmate = false;
        heuristic = calculate_board_heuristic();
    }
    heuristic_not_calc = false;
}
