TARGET_EXEC ?= chess

BUILD_DIR ?= ./build
SRC_DIRS ?= ./src

CXX := g++

SRCS := $(shell find $(SRC_DIRS) -name *.cpp -or -name *.c -or -name *.s)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := $(shell find $(SRC_DIRS) -type d)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CPPFLAGS ?= $(INC_FLAGS) -Wall -Werror -MMD -MP -O2 -DNDEBUG $(COMPILE_FLAGS)

#DEBUG_FLAGS:= -g -O0 -UNDEBUG
#PROFILE_FLAGS:= -pg -g

#ASAN:= -fsanitize=address
#UBSAN:= -fsanitize=undefined -DUBSAN_OPTIONS=print_stacktrace=1

COMPILE_FLAGS:= $(DEBUG_FLAGS) $(PROFILE_FLAGS) $(ASAN) $(UBSAN)
LINK_FLAGS:= $(PROFILE_FLAGS) -lstdc++fs


$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	$(CXX) $(OBJS) $(LINK_FLAGS) -o $@ $(LDFLAGS)

# assembly
$(BUILD_DIR)/%.s.o: %.s
	$(MKDIR_P) $(dir $@)
	$(AS) $(ASFLAGS) -c $< -o $@

# c source
$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@


.PHONY: clean

clean:
	$(RM) -r $(BUILD_DIR)

-include $(DEPS)

MKDIR_P ?= mkdir -p
